(ns kit.assert
  "Extended Assertion utilities

  `kit.assert` offers the ability to register new assertion
  identifiers with associated metadata.

  These assertions are basically clojure.lang.ExceptionInfo exceptions
  with appropriate `ex-info` about the condition.

  There are special purpose assertion macros such as `assert-empty!`
  as well as assertion catcher/accessors such as `assertion-message`.")

(defmacro assert!
  "Assert the truthiness of an expression, and raise an ExceptionInfo
  exception if that fails. Supports multiple call scenarios:

  (assert! (pos? -10) :not-positive-number)
  (assert! (pos? -10) :not-positive-number {:detail \"bad number\"})
  (assert! (pos? -10) \"Not a positive number\" {:detail \"bad number\"})
  "
  ([expr id]
   `(assert! ~expr ~id {}))
  ([expr id info-map]
   `(or ~expr
        (throw (ex-info ~id ~info-map)))))

(defn catch-assertion-value
  "Evaluate function `f` catching any ExceptionInfo conditions, and
  apply `k-fn` on the exception's `ex-data`."
  [f k-fn]
  (try
    (f)
    (catch clojure.lang.ExceptionInfo ex
      (k-fn (-> ex ex-data)))))

(defmacro assert-empty!
  "Assert that `thing` is empty, additional `args` are passed to
  `assert!`"
  [thing & args]
  `(assert! (empty? ~thing) ~@args))

(defmacro assert-not-empty!
  "Assert that `thing` is not-empty, additional `args` are passed to
  `assert!`"
  [thing & args]
  `(assert! (not (empty? ~thing)) ~@args))
