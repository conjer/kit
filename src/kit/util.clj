(ns kit.util
  (:require
   [clojure.string :as str]
   [clojure.edn :as edn]
   [clojure.java.io :as io]
   [clojure.pprint :as pp]
   [jsonista.core :as json]
   [camel-snake-kebab.core :as csk])
  (:import
   [java.util Base64 Date UUID]
   [java.text SimpleDateFormat]
   [java.io PushbackReader]
   [java.net URLEncoder URLDecoder]))

(defmacro ignore-errors
  "Evaluate body and return `nil` if any exception occurs."
  [& body]
  `(try
     ~@body
     (catch Throwable e# nil)))

(defn slurp-edn [path]
  (-> path
      slurp
      edn/read-string))

(defn spit-edn [path obj]
  (with-open [out (io/writer path)]
    (pp/pprint obj out)))

(defn base64-encode
  ([s]
   (base64-encode s :basic))
  ([s type]
   (let [basic-encoder (Base64/getEncoder)
         encoder {:basic basic-encoder
                  :url (Base64/getUrlEncoder)
                  :mime (Base64/getMimeEncoder)}]
     (.encodeToString ^java.util.Base64$Encoder (encoder type basic-encoder)
                      s))))

(defn base64-encode-str [^String s & args]
  (apply base64-encode (.getBytes s) args))

(defn base64-decode [s]
  (let [^java.util.Base64$Decoder decoder (Base64/getDecoder)]
    (.decode decoder s)))

(defn base64-decode-str [s]
  (String. (base64-decode (.getBytes s))))

(defn safe-pr-str [o]
  (binding [*print-readably* true
            *print-meta* false
            *print-dup* false]
    (pr-str o)))

(defn read-json
  ([str]
   (read-json str nil))
  ([str object-mapper-opts]
   (json/read-value str
                    (json/object-mapper object-mapper-opts))))

(defn read-json-keywords [str]
  (read-json str {:decode-key-fn keyword}))

(defn read-json-kebab-keywords [str]
  (read-json str {:decode-key-fn csk/->kebab-case-keyword}))

(defn write-json
  ([thing]
   (write-json thing nil))
  ([thing object-mapper-opts]
   (json/write-value-as-string thing
                               (json/object-mapper object-mapper-opts))))

(defn write-json-kebab-keywords [thing]
  (write-json thing {:encode-key-fn csk/->kebab-case-keyword}))

(defn now
  "Return the current timestamp as java.util.Date"
  [] (Date.))

(defn now+
  "Return a timestamp relative to current timestamp as java.util.Date,

  ;; an hour back
  (now+ -3600)

  ;; an hour from now
  (now+ 3600)"
  ([delta-secs]
   (now+ delta-secs (now)))
  ([delta-secs from]
   (Date. (+ (.getTime from) (* 1000 delta-secs)))))

(defn now-ms []
  (System/currentTimeMillis))

(defn instant? [x]
  (instance? Date x))

(defn timezone
  "Looks up a java.util.TimeZone instance by name"
  [tz]
  (if (string? tz)
    (java.util.TimeZone/getTimeZone ^String tz)
    tz))

(defn simple-date-formatter
  "Create a java.text.SimpleDateFormat with `format-string` and
  optional `tz` timezone."
  (^SimpleDateFormat [format-string]
   (simple-date-formatter format-string "UTC"))
  (^SimpleDateFormat [format-string tz]
   (doto (SimpleDateFormat. format-string)
     (.setTimeZone (timezone tz)))))

(def ^:constant iso-8601-formatter
  (simple-date-formatter "yyyy-MM-dd'T'HH:mm:ss.SSSZ"))

(defn as-formatter [formatter-args]
  (let [[formatter & _] formatter-args]
    (if (instance? SimpleDateFormat formatter)
      formatter
      (apply simple-date-formatter formatter-args))))

(defn parse-timestamp
  "Parse a timestamp string into a java.util.Date, e.g.,

  (parse-timestamp \"2001-01-01T12:34:56.789+0000\")
  => #inst \"2001-01-01T12:34:56.789-00:00\"

  (def formatter (simple-date-formatter \"MM/dd/yyyy HH:mm:ss\" \"America/Los_Angeles\"))
  (parse-timestamp \"01/01/2001 12:34:56\" formatter)
  => #inst \"2001-01-01T20:34:56.000-00:00\"

  (parse-timestamp \"01/01/2001\" \"MM/dd/yyyy\")
  => #inst \"2001-01-01T00:00:00.000-00:00\"

  (parse-timestamp \"01/01/2001 12:34:56\" \"MM/dd/yyyy HH:mm:ss\" \"America/Los_Angeles\")
  => #inst \"2001-01-01T20:34:56.000-00:00\""
  {:arglists '([timestamp-str]
               [timestamp-str formatter]
               [timestamp-str format-str]
               [timestamp-str format-str timezone])}
  ([timestamp-str]
   (parse-timestamp timestamp-str iso-8601-formatter))
  ([timestamp-str & formatter-args]
   (.parse (as-formatter formatter-args) timestamp-str)))

(defn format-timestamp
  "Format a timestamp using a SimpleDateFormat, by defaults this
  function formats as ISO 8601 timestamp string, e.g.,

  (format-timestamp)
  => \"2017-04-12T17:18:37.363+0000\"

  (format-timestamp (System/currentTimeMillis))
  => \"2017-04-12T17:18:37.363+0000\"

  (format-timestamp (System/currentTimeMillis) \"yyyy/MM\")
  => \"2017/04\"
  "
  ([]
   (format-timestamp (now-ms)))
  ([timestamp]
   (format-timestamp timestamp iso-8601-formatter))
  ([timestamp & formatter-args]
   (.format (as-formatter formatter-args) timestamp)))

(defn uuid-str [] (str (UUID/randomUUID)))

(defn read-string-safely
  "Call `read-string` if `s` is non-blank, with `*read-eval*`
  disabled."
  [s]
  (binding [*read-eval* false]
    (when (and (string? s) (not (str/blank? s)))
      (read-string s))))

(defn index-by
  "Index a sequence of maps (S) using KEY, and return a map which
  is {(key m1) m1, (key m2) m2 ...}"
  [f s]
  (reduce (fn [indexed m]
            (assoc indexed (f m) m))
          {}
          s))

(defn unscope-keyword
  "Return the unscoped (namespace removed) form of keyword K, e.g.,

  (unscope-keyword :kit.util.core/foo)
  => :foo"
  [k]
  (if-not (namespace k)
    k
    (keyword (name k))))

(defn as-coll
  "Return a 1-element collection of x if x is not already collection,
  else return x.

  (as-coll [1 2 3]) => [1 2 3]
  (as-coll 1) => [1]
  "
  [x]
  (if (or (nil? x) (sequential? x))
    x
    [x]))

(defn pushback-reader
  "Wrap a input source (reader or input-stream) in a PushbackReader
  which can be used with the Clojure `read` fn."
  [src]
  (PushbackReader. (io/reader src)))

(defn update-if
  "Conditionally `(apply update f m k args)` if the predicate
   `(p m k)` returns true. e.g.,

  (update-if not-empty {:foo \"abc\"} :foo clojure.string/upper-case)
  => {:foo \"ABC\"}

  (update-if #(clojure.string/starts-with? % \"xyz\")
             {:foo \"abc\" :bar \"xyz\"
             :foo
             clojure.string/upper-case)
  => {:foo \"abc\" :bar \"XYZ\"}"
  [p m k f & args]
  (if (p (k m))
    (apply update m k f args)
    m))

(def update-if-some? (partial update-if some?))
(def update-if-empty? (partial update-if empty?))
(def update-if-not-empty? (partial update-if not-empty))

(defn empty-string? [s]
  (and (string? s) (zero? (count s))))

(defn dissoc-if [p m]
  (->> m
       (remove #(p (val %)))
       (into (empty m))))

(def dissoc-if-nil? (partial dissoc-if nil?))
(def dissoc-if-empty-string? (partial dissoc-if empty-string?))

(defn assoc-if
  "Conditionally `assoc` k v into m if the predicate `(p v)` returns true.
  e.g.,

  (assoc-if some? {:foo 1} :bar nil)
  => {:foo 1}
  (assoc-if some? {:foo 1} :bar 2)
  => {:foo 1 :bar 2}"
  [p m k v]
  (if (p v) (assoc m k v) m))

(def assoc-if-some? (partial assoc-if some?))
(def assoc-if-not-empty? (partial assoc-if not-empty))

(defn regex?
  "Return true if the specified object `x` is a Java regular expression
  pattern."
  [x]
  (instance? java.util.regex.Pattern x))

(defmacro defn-memo [name & rest]
  `(def ~name (memoize (fn ~name ~@rest))))

(defn url-encode
  ([s] (url-encode s "UTF-8"))
  ([^String s ^String enc] (URLEncoder/encode s enc)))

(defn url-decode
  ([s] (url-decode s "UTF-8"))
  ([s enc] (URLDecoder/decode s enc)))

(defn csv-seq->map-seq
  ([csv-seq]
   (csv-seq->map-seq csv-seq {:key-fn #(keyword (url-encode %))}))
  ([csv-seq {:keys [key-fn]}]
   (let [[header & rows] csv-seq
         header (map key-fn header)]
     (mapv #(zipmap header %) rows))))

(defn map-seq->csv-seq
  ([map-seq]
   (map-seq->csv-seq map-seq nil))
  ([map-seq ks]
   (let [headers (or ks (sort (keys (first map-seq))))]
     `[~(map #(url-decode (name %)) headers)
       ~@(map (fn [row]
                (map (fn [k] (get row k)) headers))
              map-seq)])))

(defn select-vals
  "Select only values of keys specified in KS from map M"
  [m ks]
  (reduce (fn [s k] (conj s (k m)))
          []
          ks))

(defmacro with-err-str [& body]
  `(let [s# (new java.io.StringWriter)]
     (binding [*err* s#]
       ~@body
       (str s#))))

(defn shallow-transform-keys [transformer m]
  (reduce (fn [m1 [k v]]
            (assoc m1 (transformer k) v))
          {}
          m))

(defn narrow-remap-keys
  "Narrow the shape of a-map and rename keys according to key-map, e.g.,

  (narrow-remap-keys {:bar :Foo :foo :Bar} {:Foo 1 :Bar 2 :Baz 3})
  => {:bar 1, :foo 2}
  "
  [key-map a-map]
  (reduce (fn [m [target-key source-key]]
            (assoc m (keyword target-key) (source-key a-map)))
          {}
          key-map))

(defn value-and-elapsed-time
  "Return the value of `(apply f args)` and time taken to evaluate in
  milli seconds, e.g,

  (value-and-elapsed-time
   #(do (dotimes [i 100000] (rand-int 1000)) :ok))
  => {:value :ok, :elapsed-time 6}"
  [f & args]
  (let [start (now-ms)
        value (apply f args)]
    {:value value
     :elapsed-time (- (now-ms) start)}))

(defn scrolling-seq
  "Makes a lazy-seq which wraps an external sequence which may be
  realized in chunks that are referenced via a 'scroll-index'
  token. The arg 'f' is a function which returns an array with a chunk
  of elements, and index of the next chunk or nil if there are no more
  chunks, e.g.,

  (def sseq
    (let [arr [1 2 3 4 5 6 7 8 9 10]
          chunk-size 3]
      (scrolling-seq
       (fn [index]
         (let [next-index (min (count arr)
                               (+ index chunk-size))]
           [(subvec arr index next-index)
            (when (< next-index (count arr))
              next-index)])))))
  (->> sseq (drop 2) (take 3)) ;; => (3 4 5)"
  ([f]
   (scrolling-seq f nil))
  ([f index]
   (let [[value next-index] (f index)]
     (if-not next-index
       value
       (lazy-seq
        (concat value
                (scrolling-seq f next-index)))))))

(def url-re
  #"^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]")
(defn url?
  "Returns true if 's' is a string which matches a valid URL pattern."
  [s]
  (re-find url-re s))

(defn sync-println
  "A thread synchronized `println`"
  [& args]
  (locking *out*
    (apply println args)))

(defn sync-prn
  "A thread synchronized `prn`"
  [& args]
  (locking *out*
    (apply prn args)))
