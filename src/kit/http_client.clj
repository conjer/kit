(ns kit.http-client
  (:require
   [clojure.string :as str]
   [clojure.walk :as walk]
   [jsonista.core :as json]
   [org.httpkit.client :as http]
   [kit.util :as u]
   [kit.log :as log])
  (:import
   [javax.net.ssl SSLEngine SSLParameters SNIHostName]
   [java.net URI]
   [clojure.lang ExceptionInfo]))

(defn sni-configure
  [^SSLEngine ssl-engine ^URI uri]
  (let [^SSLParameters ssl-params (.getSSLParameters ssl-engine)]
    (.setServerNames ssl-params [(SNIHostName. (.getHost uri))])
    (.setUseClientMode ssl-engine true)
    (.setSSLParameters ssl-engine ssl-params)))

(def make-client
  (memoize
   (fn []
     (http/make-client {:ssl-configurer sni-configure}))))

(def read-mapper (json/object-mapper))
(def body-mapper (json/object-mapper))

(def ^:dynamic *json-object-mapper* body-mapper)

(defmacro with-json-object-mapper [m & body]
  `(binding [*json-object-mapper* ~m]
     ~@body))

(defn as-query-str
  "Encodes a map as a query string"
  ([m]
   (as-query-str m name))
  ([m key-fn]
   (->> m
        (into (sorted-map))
        (map (fn [[k v]]
               (str (key-fn k) "=" (u/url-encode v))))
        (str/join "&"))))

(defn do-request [method url headers body]
  (log/debug {:tag "request"
              :method  method
              :url     url
              :headers headers
              :body    body})
  @(http/request {:url     url
                  :method  method
                  :client  (make-client)
                  :body    body
                  :headers headers}))

(def ^:dynamic *request-client-fn* nil)

(defmacro with-request-client-fn [client-fn & body]
  `(binding [*request-client-fn* ~client-fn]
     ~@body))

(def json-content-types-re
  (re-pattern
   (str "(application|text)/"
        "("
        (str/join "|"
                  ["json"
                   "vnd\\.api\\+json"
                   "x-javascript"
                   "x-json"])
        ")")))

(def ^:dynamic *serdes-mappings*
  [{:matcher (fn [{:keys [headers]}]
               (let [ctype (not-empty (:content-type headers))]
                 (and ctype
                      (re-find json-content-types-re ctype))))
    :serializer (fn [request]
                  (u/update-if-some? request
                                     :body
                                     json/write-value-as-string
                                     *json-object-mapper*))
    :deserializer (fn [response]
                    (u/update-if-some? response
                                       :body
                                       u/read-json
                                       {:decode-key-fn keyword}))}])

(defmacro with-serdes-mappings [mappings & body]
  `(binding [*serdes-mappings* ~mappings]
     ~@body))

(defn find-serdes [request]
  (some (fn [{:as x :keys [matcher]}]
          (and (matcher request) x))
        *serdes-mappings*))

(defn process-request-body [request]
  (if-let [{:keys [serializer]} (find-serdes request)]
    (serializer request)
    request))

(defn process-response-body [response]
  (if-let [{:keys [deserializer]} (find-serdes response)]
    (deserializer response)
    response))

(defn request [{:keys [method url headers]
                :as   req}]
  (try
    (let [default-headers  {}
          headers          (-> default-headers
                               (merge headers))
          {:as req
           :keys [body]}   (process-request-body req)
          {:keys [error]
           :as   response} ((or *request-client-fn* do-request)
                            method
                            url
                            (walk/stringify-keys headers)
                            body)]
      (log/debug (merge {:tag "response"} response))
      {:status   (if error :error :ok)
       :request  req
       :response (try
                   (process-response-body response)
                   (catch Exception ex
                     (log/error ex)
                     response))})
    (catch ExceptionInfo ex
      {:status  :failure
       :request req
       :message (str ex)
       :error   (ex-data ex)})
    (catch Exception ex
      (log/error {:tag "exception" :exception ex})
      {:status  :error
       :request req
       :message (str ex)
       :error   ex})))

(defn http-post
  ([url body]
   (http-post url nil body))
  ([url headers body]
   (request {:method  :post
             :url     url
             :headers (merge {:content-type "application/json"}
                             headers)
             :body    body})))

(defn http-patch
  ([url body]
   (http-patch url nil body))
  ([url headers body]
   (request {:method  :patch
             :url     url
             :headers (merge {:content-type "application/json"}
                             headers)
             :body    body})))

(defn http-put
  ([url body]
   (http-put url nil body))
  ([url headers body]
   (request {:method  :put
             :url     url
             :headers (merge {:content-type "application/json"}
                             headers)
             :body    body})))

(defn http-head
  ([url]
   (http-head url nil))
  ([url query]
   (http-head url query {}))
  ([url query headers]
   (request {:method  :head
             :url     (if query
                        (str url "?" (as-query-str query))
                        url)
             :headers headers})))

(defn http-get
  ([url]
   (http-get url nil))
  ([url query]
   (http-get url query {}))
  ([url query headers]
   (request {:method  :get
             :url     (if query
                        (str url "?" (as-query-str query))
                        url)
             :headers headers})))

(defn http-delete
  ([url]
   (http-delete url {}))
  ([url headers]
   (request {:method  :delete
             :url     url
             :headers headers})))
