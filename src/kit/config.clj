(ns kit.config
  "`kit.config` provides a simple facility to manage global application
  settings, which can be initialized and managed from a top level
  layer, and can be looked up and used by low level Clojure code
  without too much coupling between the producers and consumers of
  config settings.

  The key abstractions are:

  ;; `init!` which takes a map of hierarchical settings values
  (kit.config/init! {:foo \"foo\" :bar {:baz \"baz\"}})

  ;; `setting`, which looks up a key or vector of keys from the global
  ;; settings
  (kit.config/setting :foo) => \"foo\"
  (kit.config/setting [:bar :baz])  => \"baz\"")

(defonce ^{:dynamic :private} *settings* (atom nil))

(defn call-with-settings
  [settings f]
  (binding [*settings* (atom (merge @*settings* settings))]
    (f)))

(defmacro with-settings
  "Override global settings values with 'settings' within the scope of
  'body'"
  [settings & body]
  `(call-with-settings ~settings (fn [] ~@body)))

(defn init!
  "Initialize global settings with the map 'settings'"
  [settings]
  (reset! *settings* settings))

(defn settings
  "Return the currently bound config settings"
  []
  @*settings*)

(defn setting
  "Return the currently bound setting for the key (or vector of keys) 'ks'

  (setting :foo) => \"foo\"
  (setting [:foo :bar]) => \"nested config setting\""
  [ks]
  (if (coll? ks)
    (get-in @*settings* ks)
    (get @*settings* ks)))
