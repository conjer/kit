(ns kit.log-test
  (:require
   [clojure.test :refer [deftest is]]
   [kit.log :as log]))

(deftest sync-logs
  (is (= 10
         (let [cnt (count
                    (pmap (fn [i]
                            (log/info {:i i})
                            i)
                          (range 10)))]
           (log/flush 5000)
           cnt))))
