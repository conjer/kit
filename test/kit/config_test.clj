(ns kit.config-test
  (:require
   [clojure.test :refer [deftest is use-fixtures]]
   [kit.config :as config]))

(defn fixture [f]
  (config/init!
   {:foo "foo"
    :bar {:baz "baz"}})
  (f))

(use-fixtures :each #'fixture)

(deftest basic-test
  (is (= "foo" (config/setting :foo)))

  (is (= "baz" (config/setting [:bar :baz])))

  (is (nil? (config/setting :quux)))

  (is (nil? (config/setting [:quux :floo]))))

(deftest override-test
  (is (= "foo2"
         (config/with-settings {:foo "foo2"}
           (config/setting :foo))))

  (is (= "baz2"
         (config/with-settings {:bar {:baz "baz2"}}
           (config/setting [:bar :baz])))))
