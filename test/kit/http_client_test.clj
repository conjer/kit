(ns kit.http-client-test
  (:require
   [clojure.test :refer [deftest is]]
   [kit.http-client :as hc]))

(deftest find-serdes-test
  (is (hc/find-serdes {:headers {:content-type "application/json"}}))
  (is (nil? (hc/find-serdes {:headers {:content-type "text/html"}}))))

(deftest request-test
  (let [exp-body (atom nil)]
    (with-redefs [hc/do-request (fn [_ _ _ body]
                                  (reset! exp-body body)
                                  {:status 200})]
      (is (= "{\"foo\":1}"
             (do
               (hc/http-post "http://localhost/test.json"
                             {:content-type "application/json"}
                             {:foo 1})
               @exp-body)))
      (is (= "foo=1&bar=2"
             (do
               (hc/http-post "http://localhost/test.json"
                             {:content-type "application/x-www-form-urlencoded"}
                             "foo=1&bar=2")
               @exp-body))))))

(deftest response-test
  (with-redefs [hc/do-request (constantly
                               {:headers {:content-type "application/json"}
                                :body "{\"foo\":1}"})]
    (is (= {:foo 1}
           (get-in (hc/http-get "http://localhost/test.json")
                   [:response :body]))))

  (with-redefs [hc/do-request (constantly
                               {:headers {:content-type "text/html"}
                                :body "<html></html>"})]
    (is (= "<html></html>"
           (get-in (hc/http-get "http://localhost/test.html")
                   [:response :body])))))
